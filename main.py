# Description:
#   Boat Alarm - an application to monitor various boat conditions and send alarms.
#
#   This is the Boat Alarm service which is responsible for receiving
#   alarms from the client and generating email notifications. This
#   implementation runs on Gooogle App Engine, but you can use any
#   service capable of parsing the alarm data.  See
#   http://boatalarm.appspot.com/ for details.
#
# License:
#   Copyright (C) 2015 Alan Noble.
#
#   This file is part of Boat Alarm. Boat Alarm is free software: you can
#   redistribute it and/or modify it under the terms of the GNU
#   General Public License as published by the Free Software
#   Foundation, either version 3 of the License, or (at your option)
#   any later version.
#
#   BoatAlarm is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with Boat Alarm in gpl.txt.  If not, see
#   <http://www.gnu.org/licenses/>.

import os
import sys
import re
import urllib
import datetime
import random
import base64
import logging
import json
import jinja2
import webapp2

# AppEngine modules
from google.appengine.ext import ndb
from google.appengine.api import mail

###############################################################################
# Globals

VERSION = "1.0"
EMAIL_SENDER = 'boatalarmservice@gmail.com'
EMAIL_SUBJECT = 'URGENT: Boat Alarm'
MAX_REGISTRATIONS = 10
LOG_RECORDS = 10
MIN_PERIOD = 1 # hours
DISCLAIMER = 'Boat Alarm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the <a href="http://www.gnu.org/licenses/">GNU General Public License</a> for more details.'
ERRORS = {
  'InvalidEmail': 'Invalid email address.',
  'InvalidCredentials': 'Invalid key or vessel name.',
  'InvalidActionData': 'Invalid action data.',
  'InvalidPeriod': 'The minimum notification period is 1 hour.',
  'MissingCredentials': 'Missing key or vessel name.',
  'IncompleteRegistration': 'Skipper and vessel are both required for registration.',
  'ExistingRegistration': 'Vessel already registered.',
  'InactiveRegistration': 'Inactive registration.',
  'RecentlyNotified': 'Already notified recently.'
}
NAV_BAR = [{'title': 'Home',     'url': '/',         'active':False },
           {'title': 'Register', 'url': '/register', 'active':False },
           {'title': 'Update',   'url': '/update',   'active':False },
           {'title': 'Help',     'url': '/help',     'active':False }]
COUNTRIES = [
  {'code':'AF', 'name':"AFGHANISTAN"},
  {'code':'AL', 'name':"ALBANIA"},
  {'code':'DZ', 'name':"ALGERIA"},
  {'code':'AS', 'name':"AMERICAN SAMOA"},
  {'code':'AD', 'name':"ANDORRA"},
  {'code':'AO', 'name':"ANGOLA"},
  {'code':'AQ', 'name':"ANTARCTICA"},
  {'code':'AG', 'name':"ANTIGUA AND BARBUDA"},
  {'code':'AR', 'name':"ARGENTINA"},
  {'code':'AM', 'name':"ARMENIA"},
  {'code':'AW', 'name':"ARUBA"},
  {'code':'AU', 'name':"AUSTRALIA"},
  {'code':'AT', 'name':"AUSTRIA"},
  {'code':'AZ', 'name':"AZERBAIJAN"},
  {'code':'BS', 'name':"BAHAMAS"},
  {'code':'BH', 'name':"BAHRAIN"},
  {'code':'BD', 'name':"BANGLADESH"},
  {'code':'BB', 'name':"BARBADOS"},
  {'code':'BY', 'name':"BELARUS"},
  {'code':'BE', 'name':"BELGIUM"},
  {'code':'BZ', 'name':"BELIZE"},
  {'code':'BJ', 'name':"BENIN"},
  {'code':'BM', 'name':"BERMUDA"},
  {'code':'BT', 'name':"BHUTAN"},
  {'code':'BO', 'name':"BOLIVIA"},
  {'code':'BA', 'name':"BOSNIA AND HERZEGOVINA"},
  {'code':'BW', 'name':"BOTSWANA"},
  {'code':'BV', 'name':"BOUVET ISLAND"},
  {'code':'BR', 'name':"BRAZIL"},
  {'code':'IO', 'name':"BRITISH INDIAN OCEAN TERRITORY"},
  {'code':'BN', 'name':"BRUNEI DARUSSALAM"},
  {'code':'BG', 'name':"BULGARIA"},
  {'code':'BF', 'name':"BURKINA FASO"},
  {'code':'BI', 'name':"BURUNDI"},
  {'code':'KH', 'name':"CAMBODIA"},
  {'code':'CM', 'name':"CAMEROON"},
  {'code':'CA', 'name':"CANADA"},
  {'code':'CV', 'name':"CAPE VERDE"},
  {'code':'KY', 'name':"CAYMAN ISLANDS"},
  {'code':'CF', 'name':"CENTRAL AFRICAN REPUBLIC"},
  {'code':'TD', 'name':"CHAD"},
  {'code':'CL', 'name':"CHILE"},
  {'code':'CN', 'name':"CHINA"},
  {'code':'CX', 'name':"CHRISTMAS ISLAND"},
  {'code':'CC', 'name':"COCOS (KEELING) ISLANDS"},
  {'code':'CO', 'name':"COLOMBIA"},
  {'code':'KM', 'name':"COMOROS"},
  {'code':'CG', 'name':"CONGO"},
  {'code':'CD', 'name':"THE DEMOCRATIC REPUBLIC OF THE CONGO"},
  {'code':'CK', 'name':"COOK ISLANDS"},
  {'code':'CR', 'name':"COSTA RICA"},
  {'code':'CI', 'name':"COTE D'IVOIRE"},
  {'code':'HR', 'name':"CROATIA"},
  {'code':'CU', 'name':"CUBA"},
  {'code':'CY', 'name':"CYPRUS"},
  {'code':'CZ', 'name':"CZECH REPUBLIC"},
  {'code':'DK', 'name':"DENMARK"},
  {'code':'DJ', 'name':"DJIBOUTI"},
  {'code':'DM', 'name':"DOMINICA"},
  {'code':'DO', 'name':"DOMINICAN REPUBLIC"},
  {'code':'EC', 'name':"ECUADOR"},
  {'code':'EG', 'name':"EGYPT"},
  {'code':'SV', 'name':"EL SALVADOR"},
  {'code':'GQ', 'name':"EQUATORIAL GUINEA"},
  {'code':'ER', 'name':"ERITREA"},
  {'code':'EE', 'name':"ESTONIA"},
  {'code':'ET', 'name':"ETHIOPIA"},
  {'code':'FK', 'name':"FALKLAND ISLANDS (MALVINAS)"},
  {'code':'FO', 'name':"FAROE ISLANDS"},
  {'code':'FJ', 'name':"FIJI"},
  {'code':'FI', 'name':"FINLAND"},
  {'code':'FR', 'name':"FRANCE"},
  {'code':'GF', 'name':"FRENCH GUIANA"},
  {'code':'PF', 'name':"FRENCH POLYNESIA"},
  {'code':'TF', 'name':"FRENCH SOUTHERN TERRITORIES"},
  {'code':'GA', 'name':"GABON"},
  {'code':'GM', 'name':"GAMBIA"},
  {'code':'GE', 'name':"GEORGIA"},
  {'code':'DE', 'name':"GERMANY"},
  {'code':'GH', 'name':"GHANA"},
  {'code':'GI', 'name':"GIBRALTAR"},
  {'code':'GR', 'name':"GREECE"},
  {'code':'GL', 'name':"GREENLAND"},
  {'code':'GD', 'name':"GRENADA"},
  {'code':'GP', 'name':"GUADELOUPE"},
  {'code':'GU', 'name':"GUAM"},
  {'code':'GT', 'name':"GUATEMALA"},
  {'code':'GN', 'name':"GUINEA"},
  {'code':'GW', 'name':"GUINEA-BISSAU"},
  {'code':'GY', 'name':"GUYANA"},
  {'code':'HT', 'name':"HAITI"},
  {'code':'HM', 'name':"HEARD ISLAND AND MCDONALD ISLANDS"},
  {'code':'HN', 'name':"HONDURAS"},
  {'code':'HK', 'name':"HONG KONG"},
  {'code':'HU', 'name':"HUNGARY"},
  {'code':'IS', 'name':"ICELAND"},
  {'code':'IN', 'name':"INDIA"},
  {'code':'ID', 'name':"INDONESIA"},
  {'code':'IR', 'name':"IRAN, ISLAMIC REPUBLIC OF"},
  {'code':'IQ', 'name':"IRAQ"},
  {'code':'IE', 'name':"IRELAND"},
  {'code':'IL', 'name':"ISRAEL"},
  {'code':'IT', 'name':"ITALY"},
  {'code':'JM', 'name':"JAMAICA"},
  {'code':'JP', 'name':"JAPAN"},
  {'code':'JO', 'name':"JORDAN"},
  {'code':'KZ', 'name':"KAZAKHSTAN"},
  {'code':'KE', 'name':"KENYA"},
  {'code':'KI', 'name':"KIRIBATI"},
  {'code':'KP', 'name':"KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF"},
  {'code':'KR', 'name':"KOREA, REPUBLIC OF"},
  {'code':'KW', 'name':"KUWAIT"},
  {'code':'KG', 'name':"KYRGYZSTAN"},
  {'code':'LA', 'name':"LAO PEOPLE'S DEMOCRATIC REPUBLIC"},
  {'code':'LV', 'name':"LATVIA"},
  {'code':'LB', 'name':"LEBANON"},
  {'code':'LS', 'name':"LESOTHO"},
  {'code':'LR', 'name':"LIBERIA"},
  {'code':'LY', 'name':"LIBYAN ARAB JAMAHIRIYA"},
  {'code':'LI', 'name':"LIECHTENSTEIN"},
  {'code':'LT', 'name':"LITHUANIA"},
  {'code':'LU', 'name':"LUXEMBOURG"},
  {'code':'MO', 'name':"MACAO"},
  {'code':'MK', 'name':"MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF"},
  {'code':'MG', 'name':"MADAGASCAR"},
  {'code':'MW', 'name':"MALAWI"},
  {'code':'MY', 'name':"MALAYSIA"},
  {'code':'MV', 'name':"MALDIVES"},
  {'code':'ML', 'name':"MALI"},
  {'code':'MT', 'name':"MALTA"},
  {'code':'MH', 'name':"MARSHALL ISLANDS"},
  {'code':'MQ', 'name':"MARTINIQUE"},
  {'code':'MR', 'name':"MAURITANIA"},
  {'code':'MU', 'name':"MAURITIUS"},
  {'code':'YT', 'name':"MAYOTTE"},
  {'code':'MX', 'name':"MEXICO"},
  {'code':'FM', 'name':"MICRONESIA, FEDERATED STATES OF"},
  {'code':'MD', 'name':"MOLDOVA, REPUBLIC OF"},
  {'code':'MC', 'name':"MONACO"},
  {'code':'MN', 'name':"MONGOLIA"},
  {'code':'ME', 'name':"MONTENEGRO"},
  {'code':'MS', 'name':"MONTSERRAT"},
  {'code':'MA', 'name':"MOROCCO"},
  {'code':'MZ', 'name':"MOZAMBIQUE"},
  {'code':'MM', 'name':"MYANMAR"},
  {'code':'NA', 'name':"NAMIBIA"},
  {'code':'NR', 'name':"NAURU"},
  {'code':'NP', 'name':"NEPAL"},
  {'code':'NL', 'name':"NETHERLANDS"},
  {'code':'AN', 'name':"NETHERLANDS ANTILLES"},
  {'code':'NC', 'name':"NEW CALEDONIA"},
  {'code':'NZ', 'name':"NEW ZEALAND"},
  {'code':'NI', 'name':"NICARAGUA"},
  {'code':'NE', 'name':"NIGER"},
  {'code':'NG', 'name':"NIGERIA"},
  {'code':'NU', 'name':"NIUE"},
  {'code':'NF', 'name':"NORFOLK ISLAND"},
  {'code':'MP', 'name':"NORTHERN MARIANA ISLANDS"},
  {'code':'NO', 'name':"NORWAY"},
  {'code':'OM', 'name':"OMAN"},
  {'code':'PK', 'name':"PAKISTAN"},
  {'code':'PW', 'name':"PALAU"},
  {'code':'PS', 'name':"PALESTINIAN TERRITORY, OCCUPIED"},
  {'code':'PA', 'name':"PANAMA"},
  {'code':'PG', 'name':"PAPUA NEW GUINEA"},
  {'code':'PY', 'name':"PARAGUAY"},
  {'code':'PE', 'name':"PERU"},
  {'code':'PH', 'name':"PHILIPPINES"},
  {'code':'PN', 'name':"PITCAIRN"},
  {'code':'PL', 'name':"POLAND"},
  {'code':'PT', 'name':"PORTUGAL"},
  {'code':'PR', 'name':"PUERTO RICO"},
  {'code':'QA', 'name':"QATAR"},
  {'code':'RE', 'name':"REUNION"},
  {'code':'RO', 'name':"ROMANIA"},
  {'code':'RU', 'name':"RUSSIAN FEDERATION"},
  {'code':'RW', 'name':"RWANDA"},
  {'code':'SH', 'name':"SAINT HELENA"},
  {'code':'KN', 'name':"SAINT KITTS AND NEVIS"},
  {'code':'LC', 'name':"SAINT LUCIA"},
  {'code':'PM', 'name':"SAINT PIERRE AND MIQUELON"},
  {'code':'VC', 'name':"SAINT VINCENT AND THE GRENADINES"},
  {'code':'WS', 'name':"SAMOA"},
  {'code':'SM', 'name':"SAN MARINO"},
  {'code':'ST', 'name':"SAO TOME AND PRINCIPE"},
  {'code':'SA', 'name':"SAUDI ARABIA"},
  {'code':'SN', 'name':"SENEGAL"},
  {'code':'RS', 'name':"SERBIA"},
  {'code':'SC', 'name':"SEYCHELLES"},
  {'code':'SL', 'name':"SIERRA LEONE"},
  {'code':'SG', 'name':"SINGAPORE"},
  {'code':'SK', 'name':"SLOVAKIA"},
  {'code':'SI', 'name':"SLOVENIA"},
  {'code':'SB', 'name':"SOLOMON ISLANDS"},
  {'code':'SO', 'name':"SOMALIA"},
  {'code':'ZA', 'name':"SOUTH AFRICA"},
  {'code':'GS', 'name':"SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS"},
  {'code':'ES', 'name':"SPAIN"},
  {'code':'LK', 'name':"SRI LANKA"},
  {'code':'SD', 'name':"SUDAN"},
  {'code':'SR', 'name':"SURINAME"},
  {'code':'SJ', 'name':"SVALBARD AND JAN MAYEN"},
  {'code':'SZ', 'name':"SWAZILAND"},
  {'code':'SE', 'name':"SWEDEN"},
  {'code':'CH', 'name':"SWITZERLAND"},
  {'code':'SY', 'name':"SYRIAN ARAB REPUBLIC"},
  {'code':'TW', 'name':"TAIWAN"},
  {'code':'TJ', 'name':"TAJIKISTAN"},
  {'code':'TZ', 'name':"TANZANIA, UNITED REPUBLIC OF"},
  {'code':'TH', 'name':"THAILAND"},
  {'code':'TL', 'name':"TIMOR-LESTE"},
  {'code':'TG', 'name':"TOGO"},
  {'code':'TK', 'name':"TOKELAU"},
  {'code':'TO', 'name':"TONGA"},
  {'code':'TT', 'name':"TRINIDAD AND TOBAGO"},
  {'code':'TN', 'name':"TUNISIA"},
  {'code':'TR', 'name':"TURKEY"},
  {'code':'TM', 'name':"TURKMENISTAN"},
  {'code':'TC', 'name':"TURKS AND CAICOS ISLANDS"},
  {'code':'TV', 'name':"TUVALU"},
  {'code':'UG', 'name':"UGANDA"},
  {'code':'UA', 'name':"UKRAINE"},
  {'code':'AE', 'name':"UNITED ARAB EMIRATES"},
  {'code':'GB', 'name':"UNITED KINGDOM"},
  {'code':'US', 'name':"UNITED STATES"},
  {'code':'UM', 'name':"UNITED STATES MINOR OUTLYING ISLANDS"},
  {'code':'UY', 'name':"URUGUAY"},
  {'code':'UZ', 'name':"UZBEKISTAN"},
  {'code':'VU', 'name':"VANUATU"},
  {'code':'VE', 'name':"VENEZUELA"},
  {'code':'VN', 'name':"VIET NAM"},
  {'code':'VG', 'name':"VIRGIN ISLANDS, BRITISH"},
  {'code':'VI', 'name':"VIRGIN ISLANDS, U.S."},
  {'code':'WF', 'name':"WALLIS AND FUTUNA"},
  {'code':'EH', 'name':"WESTERN SAHARA"},
  {'code':'YE', 'name':"YEMEN"},
  {'code':'ZM', 'name':"ZAMBIA"},
  {'code':'ZW', 'name':"ZIMBABWE"}
]
JINJA_ENV = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__), 'templates')))

###############################################################################
# Jinja2 filters

def UrlEncode(val):
  return urllib.quote(val)

def FirstWord(string):
  """Return the first word of a space-separated string."""
  parts = string.split()
  if parts and len(parts) > 0:
    return parts[0]
  else:
    return ''

JINJA_ENV.filters['urlencode'] = UrlEncode
JINJA_ENV.filters['firstword'] = FirstWord

###############################################################################
# Data store

class Registration(ndb.Model):
  """Vessel registration details."""
  key = ndb.StringProperty()
  skipper = ndb.StringProperty()
  email = ndb.StringProperty()
  vessel = ndb.StringProperty()
  date = ndb.DateTimeProperty(auto_now_add=True)
  period = ndb.IntegerProperty(default=MIN_PERIOD)
  country = ndb.StringProperty()
  timezone = ndb.FloatProperty(default=0)
  active = ndb.BooleanProperty(default=True)

def GetRegistration(key):
  """Get registration by key."""
  return Registration.get_by_id(key)

def GetOrCreateRegistration(key, **kwargs):
  """Get or create a registration, using the key as the entity id."""
  def _tx():
    reg = GetRegistration(key)
    if reg:
      return reg, False
    reg = Registration(id=key, key=key, **kwargs)
    reg.put()
    return reg, True
  return ndb.transaction(_tx)

def GetRegistrationsBySkipper(skipper):
  """Get registrations by skipper."""
  query = Registration.gql("WHERE skipper = :1", skipper)
  return query.fetch(MAX_REGISTRATIONS)

def GetRegistrationBySkipper(skipper, vessel):
  """Get registrations by skipper."""
  query = Registration.gql("WHERE skipper = :1 and vessel = :2", skipper, vessel)
  result = query.fetch(1)
  if len(result) == 0:
    return None
  else:
    return result[0]

class Alarm(ndb.Model):
  """Alarm details."""
  key = ndb.StringProperty()
  alarm = ndb.StringProperty()
  data = ndb.StringProperty()
  date = ndb.DateTimeProperty(auto_now_add=True)
  notified = ndb.BooleanProperty(default=False)

def GetAlarms(key, records=1):
  """Get alarms in reverse chronological order by key."""
  query = Alarm.gql("WHERE key = :1 ORDER BY date DESC", key)
  return query.fetch(records)

def GetLastAlarm(key, alarm, since, notified):
  """Get last alarm for given key, alarm and time."""
  query = Alarm.gql("WHERE key = :1 AND alarm = :2 AND date > DATETIME(:3) AND notified = :4", key, alarm, since.strftime("%Y-%m-%d %H:%M:%S"), notified)
  result = query.fetch(1)
  if len(result) == 0:
    return None
  else:
    return result[0]

def DeleteAlarms(key):
  """Delete alarms for given key."""
  query = Alarm.query(Alarm.key == key)
  for ent_key in query.iter(keys_only=True):
    ent_key.delete()

###############################################################################
# Utilities

def IsNumber(num):
  """Return True is string is a number (integer or floating point)."""
  if not num: return False
  pattern = re.compile(r'[+-]?[\d.]+$')
  return pattern.match(num)

def IsInteger(num):
  """Return True is string is an integer."""
  if not num: return False
  pattern = re.compile(r'[+-]?[\d]+$')
  return pattern.match(num)

def IsEmail(email):
  """Returns True if email is a valid email, False otherwise."""
  is_email = re.compile(r'^[^@]+@[^.@]+(\.[^.@]+)+$')
  return email is not None and is_email.match(email)

def FixBase64(string):
  """Fix Base 64 characters that get URL encoded."""
  if string is None:
    return ''
  string = string.replace('%2F', '/')
  string = string.replace('%20', '+')
  string = string.replace(' ', '+')
  return string

###############################################################################
# Handler base class

class BaseHandler(webapp2.RequestHandler):
  """Base class for handlers."""
     
  def RenderTemplate(self, filename, values):
    """Renders a given template with the given dictionary of values."""
    template = JINJA_ENV.get_template(filename)
    return template.render(values)

  def WriteTemplate(self, filename, values, err=None):
    """Renders and outputs a given template with the dictionary of values."""
    selected = None
    url = self.request.path
    for elt in NAV_BAR:
      elt['active'] = False
      if elt['url'] != '/' and url.find(elt['url']) >= 0:
        selected = elt
    if selected is None:
      selected = NAV_BAR[0]
    selected['active'] = True
    values.update({'request':self.request, 'nav_bar':NAV_BAR, 'disclaimer':DISCLAIMER })
    if err:
      values.update({'err':ERRORS[err]})
    self.response.out.write(self.RenderTemplate(filename, values))

  def RenderAndSendEmail(self, filename, values):
    """Renders a given email template and sends it."""
    email = mail.EmailMessage(sender=values['sender'],
                              subject = values['subject'],
                              to = values['recipient'])
    if values.get('content_type') == 'text/html':
      email.html = self.RenderTemplate(filename, values)
      email.body = ''
    else:
      email.body = self.RenderTemplate(filename, values)
    email.send()
      
###############################################################################
# Handlers
class RegisterHandler(BaseHandler):
  """Handles register requests."""
  def get(self):
    vessel = self.request.get('v')
    self.WriteTemplate('register.html', { 'vessel':vessel, 'period':MIN_PERIOD, 'countries':COUNTRIES })

  def post(self):
    skipper = self.request.get('skipper')
    email = self.request.get('email')
    vessel = self.request.get('v')
    country = self.request.get('country')
    timezone = self.request.get('timezone')
    if IsNumber(timezone):
      timezone = float(timezone)
    else:
      timezone = 0

    err = None
    if not (skipper and vessel):
      err = 'IncompleteRegistration'
    elif not IsEmail(email):
      err = 'InvalidEmail'
    else:
      reg = GetRegistrationBySkipper(skipper, vessel)
      if reg:
        if reg.active:
          err = 'ExistingRegistration'
        else:
          err = 'InactiveRegistration'

    values = { 'skipper':skipper, 'email':email, 'vessel':vessel, 'country':country, 'timezone':timezone, 'countries':COUNTRIES }
    if err:
      return self.WriteTemplate('register.html', values, err);

    created = False
    while not created:
      # generate a 12-byte long random string for the key, which is 16-long in base 64
      key = base64.b64encode("".join(map(lambda ch: chr(random.randint(0, 255)), range(12))))
      reg, created = GetOrCreateRegistration(key, skipper=skipper, email=email, vessel=vessel, country=country, timezone=timezone)

    logging.info("/register: key=%s, skipper=%s, vessel=%s" % (key, skipper, vessel))
    self.RenderAndSendEmail('register_email.txt', {'sender':EMAIL_SENDER, 'recipient':email, 'subject':'Boat Alarm registration',
                                                    'key':key, 'skipper':skipper, 'vessel':vessel})

    msg = "Your vessel is now registered with Boat Alarm. You have been emailed your key."
    values.update({ 'msg':msg })
    self.WriteTemplate('register.html', values);

class UpdateHandler(BaseHandler):
  """Handles update requests."""
  def get(self):
    key = FixBase64(self.request.get('k'))
    vessel = self.request.get('v')
    unsubscribe = self.request.get('unsubscribe')

    if key and vessel:
      reg = GetRegistration(key)
      if reg is not None and reg.key == key and reg.vessel == vessel:
        if unsubscribe == 'true':
          reg.active = False
          reg.put()
          logging.info("/update: key=%s, vessel=%s, unsubscribe=true" % (key, vessel))
          msg = 'You have been unsubscribed. You can re-activate by checking Active.'
        else:
          msg = ''
        self.WriteTemplate('update.html', { 'msg':msg, 'vessel':vessel, 'key':key, 'reg':reg })
      else:
        self.WriteTemplate('update.html', { 'vessel':vessel, 'key':key }, 'InvalidCredentials')
    else:
      self.WriteTemplate('update.html', { 'vessel':vessel, 'key':key })

  def post(self):
    key = FixBase64(self.request.get('k'))
    vessel = self.request.get('v')
    period = self.request.get('period')
    timezone = self.request.get('timezone')
    active = self.request.get('active')

    err = None
    if key and vessel:
      reg = GetRegistration(key)
      if reg is not None and reg.key == key and reg.vessel == vessel:
        if period and IsInteger(period):
          reg.period = int(period)
          if reg.period < MIN_PERIOD:
            reg.period = MIN_PERIOD
            err = 'InvalidPeriod'
        if timezone and IsNumber(timezone):
          reg.timezone = float(timezone)
        if active:
          reg.active = True
        else:
          reg.active = False
        reg.put()
        logging.info("/update: key=%s, vessel=%s, period=%d, timezone=%f, active=%d" % (key, vessel, reg.period, reg.timezone, reg.active))
        return self.WriteTemplate('update.html', { 'vessel':vessel, 'key':key, 'skipper':reg.skipper, 'reg':reg }, err)

    self.WriteTemplate('update.html', { 'vessel':vessel, 'key':key }, 'InvalidCredentials')

class AlarmHandler(BaseHandler):
  """Handles alarm requests."""

  def get(self):
    return self.post()
  
  def post(self):
    key = FixBase64(self.request.get('k'))
    vessel = self.request.get('v')
    alarm = self.request.get('a')
    alarm_data = self.request.get('ad')
    action = self.request.get('ac')
    action_data = self.request.get('acd')

    err = None
    if not key or not vessel:
      err = 'MissingCredentials'
    else:
      reg = Registration.get_by_id(key)
      if not (reg is not None and reg.key == key and reg.vessel == vessel):
        err = 'InvalidCredentials'
      elif not reg.active:
        err = 'InactiveRegistration'
          
    if not err:
      min_period = datetime.timedelta(hours=reg.period)
      now = datetime.datetime.now()

      if alarm != ':Poll' and GetLastAlarm(key, alarm, now - min_period, True):
        err = 'RecentlyNotified'
      ent = Alarm(key=key, alarm=alarm, data=alarm_data, notified=False)
      ent.put()
      logging.info("/alarm: key=%s, alarm=%s, alarm_data='%s'" % (key, alarm, alarm_data))

      if not err and action == 'email':
        self.email(reg, alarm, alarm_data, action_data)
        ent.notified = True
        ent.put()

    if not err:
      output = json.dumps({'alarm':alarm, 'alarm_data':alarm_data, 'action':action, 'action_data':action_data, 'version':VERSION})
    else:
      output = json.dumps({'error':err});

    self.response.headers['Access-Control-Allow-Origin'] = '*'
    self.response.headers['Content-Type'] = 'application/json'
    self.response.out.write(output)

  def email(self, reg, alarm, alarm_data, action_data):
    # for email, action_data is recipient or recipient:message pair
    parts = action_data.split(':')
    if len(parts) >= 1:
      recipient = parts[0]
      if len(parts) == 2:
        msg = parts[1]
      else:
        msg = ''
    else:
      return 'InvalidActionData'
    self.RenderAndSendEmail('alarm_email.txt', {'sender':EMAIL_SENDER, 'subject':EMAIL_SUBJECT, 'recipient':recipient, 'message':msg,
                                                'skipper':reg.skipper, 'alarm':alarm, 'alarm_data':alarm_data, 'key':reg.key, 'vessel':reg.vessel})
    logging.info("/alarm: emailed %s, alarm=%s, alarm_data='%s'" % (recipient, alarm, alarm_data))

class LogHandler(BaseHandler):
  """Handles log requests."""
  def get(self):
    skipper = self.request.get('skipper')
    key = FixBase64(self.request.get('k'))
    vessel = self.request.get('v')
    delete = self.request.get('delete')
    records = self.request.get('records')

    now = datetime.datetime.now()
    reg = None
    if key and vessel:
      reg = GetRegistration(key)
      if reg is not None and reg.key == key and reg.vessel == vessel:
        if delete == 'true':
          DeleteAlarms(key)
      else:
        reg = None

    elif skipper and vessel:
      reg = GetRegistrationBySkipper(skipper, vessel)

    if reg is None:
      return self.WriteTemplate('log.html', { 'vessel':vessel, 'alarms':None, 'now': now }, 'InvalidCredentials')

    if records and IsInteger(records):
     records = int(records)
    else:
      records = LOG_RECORDS

    tdelta = datetime.timedelta(hours=reg.timezone)
    now = now + tdelta
    alarms = GetAlarms(reg.key, records)
    last_alarm = None
    for alarm in alarms:
      alarm.date = alarm.date + tdelta
      if not last_alarm:
        last_alarm = alarm
    if last_alarm:
      ago = int((now - last_alarm.date).total_seconds() / 60)
    else:
      ago = ''
    self.WriteTemplate('log.html', { 'vessel':vessel, 'key':key, 'alarms':alarms, 'active':reg.active, 'now':now, 'ago':ago, 'timezone':reg.timezone })

class HelpHandler(BaseHandler):
  """Handles help requests."""
  def get(self):
    self.WriteTemplate('help.html', {})

class MainHandler(BaseHandler):
  """Handles requests to the main page."""
  def get(self):
    return self.WriteTemplate("index.html", {})

###############################################################################
# The app

app = webapp2.WSGIApplication(
  [
   ('/register', RegisterHandler),
   ('/update', UpdateHandler),
   ('/alarm', AlarmHandler),
   ('/log', LogHandler),
   ('/help', HelpHandler),
   ('/', MainHandler)
  ],
  debug=True)
